<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Requests\ProductReq;

class ProductController extends Controller
{

    public function __construct(Product $product)
    {
        $this->pageTitle = "Products Management";
        $this->model = $product;
        $this->redirectUrl = URL::route('products.index');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["lists"] = $this->model->getAllData();

        return view("products.index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = 'products.store';
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
//        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('products.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductReq $productReq)
    {//dd(Input::all());
        try{
            $this->model->addData($productReq->all());
            return redirect($this->redirectUrl)->withInput()->withErrors(["alert-success" => "Successfully added Product."]);
        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data["product"] = $product;
        $data["routeName"] = 'products.update';
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
//        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('products.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductReq $productReq, Product $product)
    {
        try{
            $this->model->updateData($productReq->all(),$productReq->get("id"));
            return redirect($this->redirectUrl)->withInput()->withErrors(["alert-success" => "Successfully Updated Product."]);
        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try{
            $this->model->deleteData($product->id);
            return redirect($this->redirectUrl)->withInput()->withErrors(["alert-success" => "Successfully Deleted Product."]);
        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
