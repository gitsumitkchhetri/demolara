<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderReq;
use App\Http\Requests\ProductReq;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class OrderController extends Controller
{
    public function __construct(Order $order,Product $product)
    {
        $this->model = $order;
        $this->product = $product;
        $this->pageTitle = "Products Management";
        $this->redirectUrl = URL::route('orders.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["lists"] = $this->model->getAllData();

        return view("orders.index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = 'orders.store';
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["products"] = [""=>"none"]+$this->product->selectRaw("CONCAT(name,' ','(',product_code,')') AS title,id")->pluck("title","id")->toArray();
        //dd($data["products"]);
        return view('orders.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(OrderReq $orderReq)
    {//dd(Input::all());
        try{
            $this->model->addData($orderReq->all());
            return redirect($this->redirectUrl)->withInput()->withErrors(["alert-success" => "Successfully added Product."]);
        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Display the specified resource.
     *
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(Order $order)
    {
        $data["product"] = $order;
        $data["routeName"] = 'products.update';
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
//        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('orders.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(OrderReq $orderReq, Order $order)
    {
        try{
            $this->model->updateData($orderReq->all(),$orderReq->get("id"));
            return redirect($this->redirectUrl)->withInput()->withErrors(["alert-success" => "Successfully Updated Product."]);
        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Order $order)
    {
        try{
            $this->model->deleteData($order->id);
            return redirect($this->redirectUrl)->withInput()->withErrors(["alert-success" => "Successfully Deleted Product."]);
        }catch (\Exception $e){
            return redirect()->back()->withInput()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
