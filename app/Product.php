<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name','description','price','product_code'];

    protected $perPage = 2;

    public function getAllData($keyword = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("name","LIKE","%$keyword%")
                    ->orWhere("email","LIKE","%$keyword%")
                    ->orWhere("phone","LIKE","%$keyword%");
            });
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){
        return $this->find($id);
    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }
}
