<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";

    protected $guarded = ['id'];
    //protected $fillable = ['']

    public function getAllData($keyword = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("name","LIKE","%$keyword%")
                    ->orWhere("oder_code","LIKE","%$keyword%");
            });
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){
        return $this->find($id);
    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public function user(){
        return $this->belongsTo("App\User","user_id","id");
    }

}
