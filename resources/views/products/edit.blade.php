@extends('master')


@section('content')

<main>

    <h1>Edit Product</h1>
    <div class="container">
        @include('errors/errors')



            {!!Form::model($product,['method'=>'PUT','route'=>[$routeName,$product->id]])!!}
                {!! Form::hidden("id",null) !!}
                <div class="panel">
                    <div class="panel-body">

                        @include('products.includes.form',['btnTxt'=>'Update'])


                        <div class="clearfix"></div>

                    </div>
                </div>
            {!!Form::close() !!}
    </div>
</main>

            <div class="clearfix"></div>



@stop
