@extends('master')


@section('content')

<main>

    <h1>Edit Orders</h1>
    <div class="container">
        @include('errors/errors')



            {!!Form::model($order,['method'=>'PUT','route'=>[$routeName,$order->id]])!!}
                {!! Form::hidden("id",null) !!}
                <div class="panel">
                    <div class="panel-body">

                        @include('orders.includes.form',['btnTxt'=>'Update'])


                        <div class="clearfix"></div>

                    </div>
                </div>
            {!!Form::close() !!}
    </div>
</main>

            <div class="clearfix"></div>



@stop
