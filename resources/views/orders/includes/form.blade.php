<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            <label class="col-sm-3 control-label require">Name</label>
                {!! Form::text("name",null,["class" => "form-control"]) !!}
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        <div class="form-group label-floating">
            <label class="col-sm-3 control-label">Product Code</label>
                {!! Form::select("user_id",$products,null,["class" => "form-control"]) !!}
        </div>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{$btnTxt}}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL)}}">Cancel</a>

    </div>
</div>




