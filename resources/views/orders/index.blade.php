@extends("master")

@section("content")

<div class="content">
    <div class="container-fluid">
        @include('errors.errors')

        <div class="row">
            <div class="col-md-12">
                    <a href="{{URL::route('orders.create')}}" class="btn btn-success" style="margin-bottom: 20px;"><span class="fa fa-plus"></span> Add</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Orders Table</h4>
                        <p class="category">Here is a subtitle for this table</p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                            <th>Name</th>
                            <th>User</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($lists as $list)
                                <tr>
                                    <td>{{$list->name}}</td>
                                    <td>{{$list->user_id}}</td>
                                    <td><a href="{{URL::route('orders.edit',["id" => $list->id])}}" class="btn btn-round btn-sm btn-success">Edit</a>
                                        {!! Form::model($list, ['method' => 'delete', 'route' => ['orders.destroy', $list->id], 'class' =>'form-inline form-delete']) !!}
                                        {!! Form::hidden('id', $list->id) !!}
                                        <button type="submit" name="delete_modal" class="btn btn-round btn-sm btn-danger" ><i class="fa fa-trash"></i> Delete </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@stop
