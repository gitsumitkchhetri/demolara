@extends('master')


@section('content')



<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Add Orders</h4>
                        <p class="category">Complete your products details</p>
                    </div>
                        <div class="card-content">
                            @include('errors/errors')
                            {!!Form::open(['method'=>'POST','route'=>$routeName])!!}
                            <div class="panel">
                                <div class="panel-body">

                                    @include('orders.includes.form',['btnTxt'=>'Save'])


                                    <div class="clearfix"></div>

                                </div>
                            </div>



                        {!!Form::close() !!}

        </div>
    </div>
</div>




@stop
