
<html>
    <head>

        <!-- BOOTSTRAP -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>

        <!-- HELPERS -->

        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/master.css')}}">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
        <div class="wrapper">

        <nav>

            <header>
                <span></span>
                {{Auth::user()->name}}
                <a href="{{URL::to('/logout')}}"><i class="fa fa-logout"></i></a>
            </header>

            <ul>
                <li><span>Navigation</span></li>
                <li><a class="active">Dashboard</a></li>
                <li><a>Statistics</a></li>
                <li><a>Roadmap</a></li>
                <li><a>Milestones</a></li>
                {{--<li><a>Tickets</a></li>--}}
                {{--<li><a>GitHub</a></li>--}}
                {{--<li><a>FAQ</a></li>--}}
                {{--<li><span>Other</span></li>--}}
                {{--<li><a>Search</a></li>--}}
                {{--<li><a>Settings</a></li>--}}
                <li>

                    <form method="POST" action="{{route('logout')}}">
                        {{ csrf_field() }}
                        <button type="submit" onclick="return confirm('Are you sure?');">Logout</button>
                    </form>
                    </li>
            </ul>

        </nav>

        @yield("content")
    </div>
    </body>
</html>

