
@php $msg = ""; @endphp
@if($errors->any())
    {{--@if( ($errors->first('alert-success') || $errors->first('alert-danger')))--}}
        {{--@foreach (['danger', 'warning', 'success', 'info'] as $msg)--}}
            <div class="card card-plain">
            @if($errors->first('alert-danger'))
                @php $msg = 'alert-danger'; @endphp
                <div class="card-header" data-background-color="red">
            @elseif($errors->first('alert-warning'))
                @php $msg = 'alert-warning'; @endphp
                <div class="card-header" data-background-color="yellow">
            @elseif($errors->first('alert-success'))
                @php $msg = 'alert-success'; @endphp
                <div class="card-header" data-background-color="green">
            @else
                @php $msg = 'alert-info'; @endphp
                <div class="card-header" data-background-color="blue">
            @endif



                    <h4 class="title">Message</h4>
                    <p class="category">{{$errors->first($msg)}}</p>
                </div>
            </div>
        {{--@endforeach--}}

    {{--@else--}}
        {{--<div class="alert alert-danger" role="alert">--}}
            {{--@foreach($errors->all() as $error)--}}
                {{--<p class=""><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span>&nbsp;{{ trans($error) }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--@endif--}}
@endif