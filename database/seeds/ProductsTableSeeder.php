<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $products = New Product();
        foreach (range(1,5) as $index) {
            $products->create([
                'name' => $faker->name,
                'description' => $faker->text,
                'product_code' =>$faker->uuid,
                'price' => $faker->numberBetween(1000, 3000),
            ]);
        }
    }
}
