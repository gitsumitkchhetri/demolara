<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('users')->where("id","=",1)->first() == null){
            DB::table('users')->insert([

                [
                    'id' => 1,
                    'name'=>'Admin',
                    'email'=>'info@demo.com',
                    'password'=> bcrypt('123admin@'),
                    'created_at'=> date("Y-m-d H:i:s")
                ]
            ]);
        }
    }
}
