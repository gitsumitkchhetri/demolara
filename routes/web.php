<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/','Auth\LoginController@showLoginForm');
Route::get('/login','Auth\LoginController@showLoginForm');
Route::post('/login','Auth\LoginController@login');
//Route::get('/logout','Auth\LoginController@logout');


include('backend.php');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth' ], function () {
    Route::resource('products', 'ProductController');
    Route::resource('orders', 'OrderController');

});

//Route::get('/home', 'DashController@index')->name('home');
//Route::get('/todo', 'ToDoController@index')->name('todo.indes');
//Route::get('/todo/create', 'ToDoController@create')->name('todo.create');
//Route::post('/todo', 'ToDoController@store')->name('todo.store');
//Route::get('/todo/{id}', 'ToDoController@show')->name('todo.show');
//Route::get('/todo/{id}/edit', 'ToDoController@edit')->name('todo.edit');
//Route::put('/todo/{id}', 'ToDoController@update')->name('todo.update');
//Route::patch('/todo/{id}', 'ToDoController@update')->name('todo.update');
//Route::delete('/todo', 'ToDoController@destroy')->name('todo.destroy');

